package com.hansae.shutdowntest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i2 = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
        i2.putExtra("android.intent.extra.KEY_CONFIRM", false);
        i2.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i2);

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "PDA Shut Down", Toast.LENGTH_SHORT).show();
                Intent i = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
                i.putExtra("android.intent.extra.KEY_CONFIRM", false);
                i.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        timer.schedule(timerTask, 60000);
    }
}
